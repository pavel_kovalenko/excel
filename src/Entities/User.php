<?php
namespace Entities;
/**
 * @Entity @Table(name="users")
 * @HasLifecycleCallbacks
 **/
class User
{
    /** @Id @Column(type="integer") @GeneratedValue **/
    protected $id;

    /** @Column(type="string") **/
    protected $email;

    /** @Column(type="string") **/
    protected $firstname;

    /** @Column(type="string") **/
    protected $lastname;

    /** @Column(type="string", nullable=true) **/
    protected $country;

    /** @Column(type="string", nullable=true) **/
    protected $city;

    /** @Column(type="string", nullable=true) **/
    protected $address;

    /** @Column(type="string") **/
    protected $status;

    /** @Column(type="string") **/
    protected $added_date;

    /** @Column(type="string") **/
    protected $password;

    public function getId()
    {
        return $this->id;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
    }

    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }

    public function setCountry($country)
    {
        $this->country = $country;
    }

    public function setCity($city)
    {
        $this->city = $city;
    }

    public function setAddress($address)
    {
        $this->address = $address;
    }

    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     *  @PrePersist
     */
    public function doStuffOnPrePersist()
    {
        $this->added_date = date('Y-m-d H:i:s');
    }
}