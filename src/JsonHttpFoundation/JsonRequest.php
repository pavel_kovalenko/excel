<?php
namespace JsonHttpFoundation;
use Symfony\Component\HttpFoundation\Request;

class JsonRequest extends  Request {
    public function __construct(array $query = array(), array $request = array(), array $attributes = array(), array $cookies = array(), array $files = array(), array $server = array(), $content = null)
    {
        parent::__construct($query, $request, $attributes, $cookies, $files, $server, $content);

        if (0 === strpos($this->headers->get('Content-Type'), 'application/json')) {
            $data = json_decode($this->getContent(), true);

            $this->request->replace(is_array($data) ? $data : array());
        }
    }
}