(function() {
    'use strict';

    angular.module('app')
        .controller('MappingCtrl', ['$scope', '$http', 'constant', 'dataService', '$location',
            function ($scope, $http, constant, dataService, $location) {

                var fileData = dataService.getFileData(),
                    columns = dataService.getColumns();


                if (!fileData) {
                    $location.path('/');
                }

                $scope.fileData = fileData;
                $scope.error = '';

                var formData = {
                    password: fileData.password,
                    status: fileData.status,
                    columns: columns
                };

                $scope.formData = formData;

                $scope.doMapping = function(data) {
                    $scope.error = '';
                    for (var i = 0; i < constant.fields.length; i++) {
                        var field = constant.fields[i];
                        if (field.required && !field.hasDefault && data.columns.indexOf(field.name)===-1) {
                            $scope.error = 'You must specify firstname, lastname and email';
                            return;
                        }
                    }

                    fileData.password = formData.password;
                    fileData.status = formData.status;
                    dataService.setFileData(fileData);

                    dataService.setColumns(formData.columns);

                    $location.path('/preview');
                }
        }]);
})();