(function() {
    'use strict';

    angular.module('app')
        .controller('PreviewCtrl', ['$scope', '$http', 'constant', 'dataService', '$location',
            function ($scope, $http, constant, dataService, $location) {
                var fileData = dataService.getFileData(),
                    columns = dataService.getColumns();
                $scope.fileData = fileData;
                $scope.columns = columns;
                $scope.rowSaved = 0;
                $scope.error = '';

                $scope.save = function() {
                    $http.post(constant.apiBaseUrl+'/save.php', {
                        filename: fileData.filename,
                        columns: columns,
                        defaults: {
                            password: fileData.password,
                            status:  fileData.status
                        }
                    }).then(function(response) {
                        $scope.rowSaved = response.data.total;
                    }, function(response) {
                        $scope.error = 'error saving file';
                    });
                };

        }]);
})();