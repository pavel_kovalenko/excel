(function() {
    'use strict';

    angular.module('app')
        .controller('UploadCtrl', ['$scope', '$http', 'constant', 'dataService', '$location',
            function ($scope, $http, constant, dataService, $location) {

                $scope.formData = {
                    password: null,
                    status: null,
                    excelFile: null
                };

                $scope.uploadForm = function(formData) {
                    $http.post(constant.apiBaseUrl+'/upload.php', formData).then(function(response) {
                        dataService.setFileData(response.data);
                        dataService.setColumns(null);
                        $location.path('/mapping');
                    }, function(response) {
                        throw 'error uploading file';
                    });
                }
        }]);
})();