(function() {
    'use strict';

    angular.module('app')
        .factory('dataService', ['localStorageService', function(localStorageService) {
            var fileData = localStorageService.get('fileData'),
                columns = localStorageService.get('columns');


            var service = {
                setFileData: setFileData,
                getFileData: getFileData,
                getColumns: getColumns,
                setColumns: setColumns
            }

            return service;

            function setFileData(data) {
                fileData = data;
                localStorageService.set('fileData', fileData);
            }

            function getFileData() {
                return fileData ? fileData : null;
            }

            function setColumns(data) {
                columns = data;
                localStorageService.set('columns', columns);
            }

            function getColumns() {
                return columns ? columns : [];
            }
        }]);
})();