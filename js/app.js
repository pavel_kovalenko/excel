(function() {
    'use strict';

    angular.module('app', ['ngRoute', 'LocalStorageModule'])
        .config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
            //$locationProvider.html5Mode({
            //    enabled: true,
            //    requireBase: false
            //});

            $routeProvider.
                when('/', {
                    templateUrl: 'templates/upload-page.html',
                    controller: 'UploadCtrl as ctrl'
                }).
                when('/mapping', {
                    templateUrl: 'templates/mapping-page.html',
                    controller: 'MappingCtrl'
                }).
                when('/preview', {
                    templateUrl: 'templates/preview-page.html',
                    controller: 'PreviewCtrl'
                }).
                otherwise({
                    redirectTo: '/'
                });
        }]);
})();