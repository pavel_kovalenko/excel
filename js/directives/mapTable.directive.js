(function() {
    'use strict';

    angular.module('app')
        .directive("mapTable", ['constant', function (constan) {
            return {
                scope: {
                    mapTableData: "=",
                    mapTableModel: "="
                },
                templateUrl: 'js/directives/map-table.html',
                link: function (scope, element, attributes) {
                    if (!scope.mapTableData || !scope.mapTableData.rows || !scope.mapTableData.rows.length) {
                        return;
                    }

                    scope.fields = constan.fields;
                    if (!scope.mapTableModel.length) {
                        angular.forEach(scope.mapTableData.rows[0], function(cell, index) {
                            scope.mapTableModel.push(constan.fields[index].name);
                        });
                    }


                    scope.change = function(index, prevValue) {

                        var selected = scope.mapTableModel[index],
                            before = scope.mapTableModel.slice(0, index),
                            after = scope.mapTableModel.slice(index+1, scope.mapTableModel.length);

                        if (before.indexOf(selected) !== -1) {
                            scope.mapTableModel[before.indexOf(selected)] = prevValue;
                        }

                        if (after.indexOf(selected) !== -1) {
                            scope.mapTableModel[after.indexOf(selected)+index+1] = prevValue;
                        }
                    };
                }
            }
        }]);
})();


