(function() {
    'use strict';

    angular.module('app')
        .constant('constant', {
            apiBaseUrl: '/api',
            fields: [{
                name: 'firstname',
                required: true
            },{
                name: 'lastname',
                required: true
            },{
                name: 'email',
                required: true
            },{
                name: 'country',
                required: false
            },{
                name: 'city',
                required: false
            },{
                name: 'address',
                required: false
            }, {
                name: 'password',
                required: true,
                hasDefault: true
            }]
        });
})();