<?php
require_once realpath(__DIR__).'/../vendor/autoload.php';
require_once realpath(__DIR__).'/../bootstrap.php';

ini_set('display_errors', 1);
use JsonHttpFoundation\JsonRequest;
use Symfony\Component\HttpFoundation\JsonResponse;
use Entities\User;

$request = JsonRequest::createFromGlobals();
$response = new JsonResponse();
$errors = $data = array();

if (!$request->request->get('filename')) {
    $errors['filename'] = 'Filename isn\'t specified';
}

$filename = realpath(__DIR__).'/tmp/'.$request->request->get('filename');
if (!(is_file($filename) && is_readable($filename))) {
    $errors['filename'] = 'File doesn\'t exists';
}

$columns = $request->request->get('columns');
if (!$columns) {
    $errors['columns'] = 'Columns aren\'t specified';
}


try {
    $csv = new parseCSV();
    $delimiter = $csv->auto($filename);

    $inputFileType = PHPExcel_IOFactory::identify($filename);
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    $objReader->setDelimiter($delimiter);
    $objPHPExcel = $objReader->load($filename);

    $sheet = $objPHPExcel->getSheet(0);
    $batchSize = 500;
    $highestRow = $sheet->getHighestRow();
    $highestColumn = PHPExcel_Cell::columnIndexFromString($sheet->getHighestDataColumn());
    $defaults = $request->request->get('defaults');


    $entityManager->getConnection()->beginTransaction();

    $start = 1;
    do {
        $end = $start+$batchSize > $highestRow ? $highestRow : $start+$batchSize;
        $sheetData = $sheet->rangeToArray('A'.$start.':' . $sheet->getHighestDataColumn() . $end, null, true, false);

        foreach ($sheetData as $key => $row) {
            $user = new User();
            foreach($columns as $index => $columnName) {
                $cell = !empty($row[$index]) ? $row[$index] : '';
                if (!$cell && isset($defaults[$columnName])) {
                    $cell = $defaults[$columnName];
                }

                $setMethod = sprintf('set%s', ucfirst($columnName));
                if ($cell) {
                    $user->$setMethod($cell);
                }

            }

            foreach ($defaults as $columnName=>$defaultValue) {
                if(!in_array($columnName, $columns)) {
                    $setMethod = sprintf('set%s', ucfirst($columnName));
                    $user->$setMethod($defaultValue);
                }
            }

            $entityManager->persist($user);
            if (($key % $batchSize) === 0) {
                $entityManager->flush();
                $entityManager->clear();
            }

        }

        $start = $end;
    } while ($start < $highestRow);

    $entityManager->flush();
    $entityManager->getConnection()->commit();
    $entityManager->clear();

} catch(Exception $e) {
    $entityManager->getConnection()->rollback();
    $errors['excelFile'] = $e->getMessage();
}


if (!empty($errors)) {
    $response->setStatusCode(400);
    $response->setData($errors);
} else {
    $response->setData(array('total'=> $highestRow));
    $response->setStatusCode(200);
}

$response->send();