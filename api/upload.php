<?php
require_once realpath(__DIR__).'/../vendor/autoload.php';

use JsonHttpFoundation\JsonRequest;
use Symfony\Component\HttpFoundation\JsonResponse;


$request = JsonRequest::createFromGlobals();
$response = new JsonResponse();
$errors = $data = array();

if (!$request->request->get('password')) {
    $errors['password'] = 'Default password is required';
}

if (!$request->request->get('status')) {
    $errors['status'] = 'Default status is required';
}

if (!$request->request->get('excelFile')) {
    $errors['excelFile'] = 'Error of loading file';
}


$filename = tempnam(realpath(__DIR__).'/tmp/', 'csv_');
list($type, $encodedData) = explode(';', $request->request->get('excelFile'));
list(, $encodedData)      = explode(',', $encodedData);

$decodedData = base64_decode($encodedData);
file_put_contents($filename, $decodedData);

try {
    $csv = new parseCSV();
    $delimiter = $csv->auto($filename);

    $inputFileType = PHPExcel_IOFactory::identify($filename);
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    $objReader->setDelimiter($delimiter);
    $objPHPExcel = $objReader->load($filename);

    $sheet = $objPHPExcel->getSheet(0);

    $sheetInfo = array(
        'highestRow' => $sheet->getHighestRow(),
        'highestColumn' => PHPExcel_Cell::columnIndexFromString($sheet->getHighestDataColumn())
    );


    $count = $sheet->getHighestRow() < 10 ? $sheet->getHighestRow(): 3;
    $sheetInfo['rows'] = $sheet->rangeToArray('A1:' . $sheet->getHighestDataColumn() . $count, null, true, false);

} catch(Exception $e) {
    $errors['excelFile'] = $e->getMessage();
}

if (!empty($errors)) {
    $response->setStatusCode(400);
    $response->setData($errors);
} else {
    $sheetInfo['filename'] = basename($filename);
    $sheetInfo['password']  = $request->request->get('password');
    $sheetInfo['status'] = $request->request->get('status');

    $response->setStatusCode(200);
    $response->setData($sheetInfo);
}

$response->send();